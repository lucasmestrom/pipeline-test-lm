const request = require('request');


// Triggered by myTimer in function.json
module.exports = async function myfunction(context, myTimer) {
    // Authentication for DevOps and the the URL. In the URL, we only GET the builds from the last week. Otherwise we GET the entire build list which is not scalable
    // Basic authentication requires base64 encoding for both Zendesk and DevOps API
    // This can be done better, by adding the credentials somewhere else (Secret in Azure function)
    var devOpsUser = 'lucas.mestrom@itility.nl';
    var devOpsPwd = 'w2myaoltr55z2abrj52nadbebjy5ryti3zyzo5xxjjn7p6why3ta';
    var authDevOps = new Buffer.from(devOpsUser + ':' + devOpsPwd).toString('base64');
    var getUrl = 'https://dev.azure.com/lucasmestrom/Ampleon%20Project%201/_apis/build/builds';
    var today = new Date();
    var urlParameterTime = today.getDate()-7;

    // Zendesk authentication and variables
    var zendeskOrg = 'ypptest';
    var zendeskUrl = 'https://'+zendeskOrg+'.zendesk.com/api/v2/tickets.json';
    var zendeskUser = 'lucas.mestrom@itility.nl';
    var zendeskPwd = '/token:Dl59GRDCqNdgpQPeR9XfdT5KYxBVeRwbHkRYICtB';
    var authZendesk = new Buffer.from(zendeskUser + zendeskPwd).toString('base64');

    // Basic variables
    var numberOfBuilds = 0;
    var buildThreshold = 3;
    var queueDurationThreshold = 30;
    var devOpsProjectName = '';

    // These are the options that are needed for the HTTP GET request
    var options = {
        method: 'GET',
        url: getUrl,  
        qs: { 
            'api-version': '5.1',
            'minTime': urlParameterTime
        },
        headers: {
            'cache-control': 'no-cache',
            Connection: 'keep-alive',
            Host: 'dev.azure.com',
            Authorization: 'Basic ' + authDevOps
        }
    };

    // This function converts miliseconds to minutes, which is used when comparing the current time to the queuetime of a build
    function convertMiliseconds (miliseconds) {
        var total_seconds, total_minutes;

        total_seconds = parseInt(Math.floor(miliseconds/1000));
        total_minutes = parseInt(Math.floor(total_seconds/60));

        return total_minutes;
    };

    // Executes the HTTP GET Request
    request(options, function (error, response, body) { 
        if (error) context.done(error);

        // Parse the body of the JSON request in a variable
        var body_json = JSON.parse(body);
        var list_builds = body_json.value;

        // Using the for loop you go over the builds in the list you retrieved
        for (var i = 0; i < list_builds.length; i++) {
            
            //context.log(list_builds[i].status);
            
            devOpsProjectName = list_builds[i].project.name;

            // Put the queueTime object of the builds in a variable and then turn the string into a date
            var queueJSON = list_builds[i].queueTime;
            var queueStartTime = new Date(queueJSON);
            var currentTime = new Date();
            var queueDuration = convertMiliseconds(currentTime-queueStartTime);
            
            //context.log(queueDuration);

            // Go over each build to see if the status =  'notStarted', which means that the build is currently in queue
            // For each build that has not started, and has been in the queue for at least 30 minutes, up the count
            // When a threshold of the number of queued builds is reached, send out a HTPP POST to Zendesk...
            if (list_builds[i].status == 'notStarted' && queueDuration >= queueDurationThreshold) {
                
                numberOfBuilds++;           
            } 
        }

        // If the number of builds is > 3, send the payload through a HTTP Post to Zendesk
        if (numberOfBuilds > buildThreshold) {
            
            // Payload that is sent to Zendesk, with the DevOps Project name which is retrieved from the build list + the number of builds
            var payload = {
                "ticket": {
                    "subject": "Number of builds in queue is too high!", 
                    "comment": {
                        "body": "The build queue in DevOps project " + devOpsProjectName + "has contained "+ numberOfBuilds + " builds over the last half our (threshold alert value: "+ buildThreshold +"). Please discuss with customer if we need to scale up"}
                }
            };
            
            request.post({
                url: zendeskUrl, 
                body: payload, 
                headers:{
                    Authorization: 'Basic ' + authZendesk
                }, 
                json: true
            },
            
                function(error, response, body) {
                    context.log(response.statusCode);
                    context.log(body); 
                    // console.log(response.statusCode);
                    // console.log(body); 
                }
            );
        }
        context.done(null, 'Done');
    });
};